import Vuex from "vuex"
import Vue from "vue"
import moment from "moment"
import Pricing from "./models/pricing"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    booking: "",
    pricings: [
      {
        low: {
          date_from: "2018-05-15",
          date_to: "2018-06-20",
          econom: 1000,
          standart: 1500,
          lux: 2000,
          child_discount_perc: 50
        },
        high: {
          date_from: "2018-06-21",
          date_to: "2018-08-20",
          econom: 1800,
          standart: 2800,
          lux: 4000,
          child_discount_perc: 25
        },
        low2: {
          date_from: "2018-08-20",
          date_to: "2018-09-15",
          econom: 1200,
          standart: 1800,
          lux: 2300,
          child_discount_perc: 25
        }
      }
    ]
  },
  mutations: {
    setBooking(state, booking) {
      state.booking = booking
    }
  },
  getters: {
    beginDate: state => {
      return moment(state.pricings[0].low.date_from)
    },
    endDate: state => {
      return moment(state.pricings[0].low2.date_to)
    },
    pricings: state => {
      return [
        new Pricing(state.pricings[0].low),
        new Pricing(state.pricings[0].high),
        new Pricing(state.pricings[0].low2)
      ]
    },
    pricingsByRange: (state, getters) => (begin, end) => {
      begin = moment(begin)
      end = moment(end)
      var days = 0
      return getters.pricings.map(pricing => {
        let pricingBegin = moment(pricing.dateFrom)
        let pricingEnd = moment(pricing.dateTo)
        if (
          begin.isSameOrBefore(pricingEnd) &&
          end.isSameOrAfter(pricingBegin)
        ) {
          days = moment
            .duration(
              (end.isAfter(pricingEnd) ? pricingEnd : end).diff(
                begin.isBefore(pricingBegin) ? pricingBegin : begin
              )
            )
            .asDays()
        }
        return {
          ...pricing,
          days: days
        }
      })
    }
  }
})
