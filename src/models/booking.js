import moment from "moment"
export default class Booking {
  constructor(
    guestCountYoung,
    guestCountMid,
    guestCountOld,
    roomType,
    beginDate,
    endDate,
    fName,
    sName,
    mName,
    email,
    phone
  ) {
    this.guestCountYoung = guestCountYoung ? guestCountYoung : null
    this.guestCountMid = guestCountMid ? guestCountMid : null
    this.guestCountOld = guestCountOld ? guestCountOld : null
    this.roomType = roomType ? roomType : null
    this.beginDate = beginDate ? beginDate : null
    this.endDate = endDate ? endDate : null
    this.fName = fName ? fName : ""
    this.sName = sName ? sName : ""
    this.mName = mName ? mName : ""
    this.email = email ? email : ""
    this.phone = phone ? phone : ""
    this.price = 0
  }
  calculateValid() {
    if (
      (this.guestCountYoung === null &&
        this.guestCountMid === null &&
        this.guestCountOld === null) ||
      this.roomType === null ||
      this.beginDate === null ||
      this.endDate === null
    ) {
      return false
    }
    if (
      !this.validateGuestCount().valid ||
      !this.validateGuestCountOld().valid ||
      !this.validateDatesRange().valid
    ) {
      return false
    }
    return true
  }
  validateGuestCount() {
    if (this.allGuestCount() < 1) {
      return {
        valid: false,
        message: "Должен быть указан хотя бы один гость",
        fields: ["guestCountYoung", "guestCountMid", "guestCountOld"]
      }
    }
    return { valid: true }
  }
  validateGuestCountOld() {
    var guestCountYoung = this.guestCountYoung
      ? parseInt(this.guestCountYoung)
      : 0
    var guestCountMid = this.guestCountMid ? parseInt(this.guestCountMid) : 0
    var guestCountOld = this.guestCountOld ? parseInt(this.guestCountOld) : 0
    if (guestCountYoung + guestCountMid > guestCountOld * 3) {
      return {
        valid: false,
        message: "На одного взрослого не более 3 детей от 0-12 лет",
        fields: ["guestCountYoung", "guestCountMid", "guestCountOld"]
      }
    }
    return { valid: true }
  }
  validateDatesRange() {
    if (
      (this.beginDate !== null) & (this.endDate !== null) &&
      !moment(this.beginDate).isBefore(this.endDate)
    ) {
      return {
        valid: false,
        message: "Неверный диапазон дат",
        fields: ["beginDate", "endDate"]
      }
    }
    return { valid: true }
  }

  allGuestCount() {
    return (
      (this.guestCountYoung !== null ? parseInt(this.guestCountYoung) : 0) +
      (this.guestCountMid !== null ? parseInt(this.guestCountMid) : 0) +
      (this.guestCountOld !== null ? parseInt(this.guestCountOld) : 0)
    )
  }

  roomTypeAsString() {
    switch (this.roomType) {
      case 1:
        return "Эконом"
      case 2:
        return "Стандарт"
      case 3:
        return "Люкс"
      default:
        return ""
    }
  }
}
