export default {
  getMonthName(date) {
    if (date !== null) {
      switch (date.getMonth() + 1) {
        case 1:
          return "января"
        case 2:
          return "февраля"
        case 3:
          return "марта"
        case 4:
          return "апреля"
        case 5:
          return "мая"
        case 6:
          return "июня"
        case 7:
          return "июля"
        case 8:
          return "августа"
        case 9:
          return "сентября"
        case 10:
          return "октября"
        case 11:
          return "ноября"
        case 12:
          return "декабря"

        default:
          break
      }
    }
  },
  getGuestWord(num) {
    if (num !== null) {
      var lastNum = parseInt(num.toString()[num.toString().length - 1])
      if (lastNum === 1) {
        return "человека"
      }
      return "человек"
    }
  },
  getChildWord(num) {
    if (num !== null) {
      var lastNum = parseInt(num.toString()[num.toString().length - 1])
      if (lastNum === 1) return "ребенок"
      if (lastNum > 1 && lastNum < 5) return "ребенка"
      if (lastNum >= 5 || lastNum === 0) return "детей"
    }
  },
  getAdultWord(num) {
    if (num !== null) {
      var lastNum = parseInt(num.toString()[num.toString().length - 1])
      if (lastNum === 1) return "взрослый"
      return "взрослых"
    }
  }
}
