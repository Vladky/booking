export default class Pricing {
  constructor(obj) {
    this.dateFrom = obj.date_from
    this.dateTo = obj.date_to
    this.econom = obj.econom
    this.standart = obj.standart
    this.lux = obj.lux
    this.childDiscountPerc = obj.child_discount_perc
  }
}
